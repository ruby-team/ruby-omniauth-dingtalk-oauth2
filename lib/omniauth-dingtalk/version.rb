# frozen_string_literal: true

module OmniAuth
  module Dingtalk
    VERSION = '1.0.1'
  end
end
